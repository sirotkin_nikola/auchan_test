import os.path
import re
from typing import List


def w_file(num_list_write: List, current_p: str) -> None:
    success_file_name: str = re.sub(r'TEST_AUCHAN', r'TEST_AUCHAN_success', num_list_write[0])
    with open(os.path.join(current_p, 'Result', success_file_name), 'w') as suc_file:
        for result in num_list_write[1]:
            suc_file.write(f'{result}\n')
