import re
from typing import List


def r_file(path_to_file: str, file_name: str) -> List:
    total_number_list: List = []
    with open(path_to_file, 'r') as f:
        for number in f.read()[1:-1].split(','):
            if re.search(f'-', number):
                range_of_numbers = number.split('-')
                numbers_list: List = list(range(int(range_of_numbers[0]), int(range_of_numbers[1]) + 1))
                total_number_list += numbers_list
            else:
                total_number_list.append(int(number))
    return [file_name, total_number_list]