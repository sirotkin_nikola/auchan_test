import os
import re
from file_read import r_file
from file_write import w_file
from typing import List
import pathlib

current_path: str = os.getcwd()


def main_script() -> None:
    global current_path
    for dirpath, dirnames, filenames in os.walk('.'):
        if re.findall(r'.+TEST_.+', dirpath):
            folder: str = dirpath[1:]
            for file in filenames:
                file_path: str = os.path.join(current_path + folder, file)
                num_list: List = r_file(file_path, file)
                w_file(num_list, current_path)


if __name__ == '__main__':
    main_script()
